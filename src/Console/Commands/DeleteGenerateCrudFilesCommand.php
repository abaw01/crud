<?php

namespace Abaw01\Crud\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Illuminate\Http\Response;

class DeleteGenerateCrudFilesCommand extends Command
{
    protected $files;

    protected $signature = 'abaw01:crud:del {ModelName?}';
    protected $type = 'Controller';

    protected $description = 'This command admin generate';
    
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }
    
    public function fire()
    {

        $pathFileController = $this->getPathFileController($this->argument('ModelName'));
        $pathFileModel      = $this->getPathFileModel($this->argument('ModelName'));
        $pathFileRequest    = $this->getPathFileRequest();
        $pathFileRoute      = $this->getPathFileRoute();
        $pathStubRoute      = $this->getStubRoute();
        $pathFileViewList   = $this->getPathFileViewList($this->argument('ModelName'));

        $this->files->delete($pathFileController);
        $this->files->delete($pathFileModel);
        $this->replaceContent($pathFileRoute, $pathStubRoute);
        $this->files->delete($pathFileViewList.'/*.*');
        $this->files->deleteDirectory($pathFileViewList);

        $this->info('');
        $this->info($this->type.' delete successfully.');
    }

    protected function replaceContent($pathFileRoute, $pathStubRoute)
    {
        $content = $this->files->get($pathFileRoute);
        $oldContent = $this->buildContentRoute($this->argument('ModelName'), $pathStubRoute);
        $newContent = str_replace($oldContent, '', $content);
        $this->files->put($pathFileRoute, $newContent);
    }

    protected function buildContentRoute($name, $pathStub)
    {
        $stub = $this->files->get($pathStub);
        $stubLower = $this->replaceClassToLower($stub, $name);

        return $this->replaceClassDummy($stubLower, $name);
    }

    protected function getStubRoute()
    {
        return __DIR__.'/../../Skeleton/app/Http/routes.stub';
    }

    protected function replaceClassToLower($stub, $name)
    {
        $namelower = Str::lower($name);

        return str_replace('dummyclass', $namelower, $stub);
    }

    protected function replaceClassDummy($stub, $name)
    {
        $stubret = str_replace('DummyClass', $name, $stub);

        return $stubret;
    }

    protected function getPathFileController($name)
    {
        return $this->laravel['path'].'/'.'Http/Controllers/Admin/'.$name.'Controller.php';
    }

    protected function getPathFileModel($name)
    {
        return $this->laravel['path'].'/'.'Model/'.$name.'.php';
    }

    protected function getPathFileRoute()
    {
        return $this->laravel['path'].'/'.'Http/routes.php';
    }

    protected function getPathFileViewList($name)
    {
        return $this->laravel['path'].'/../resources/views/Admin/'.$name;
    }

    protected function getPathFileRequest()
    {
        return $this->laravel['path'].'/Http/Requests';
    }

    protected function replaceNamespace(&$stub, $name)
    {
        $stub = str_replace(
            'DummyNamespace', 'App\\Http\\Controllers\\Admin', $stub
        );
        $stub = str_replace(
            'DummyRootNamespace', 'App\\', $stub
        );

        return $this;
    }

    protected function getNamespace($name)
    {
        return trim(implode('\\', array_slice(explode('\\', $name), 0, -1)), '\\');
    }

    protected function getPathBaseModel($name)
    {

        return $this->laravel['path'].'/'.'Model/Base/'.$name.'.php';
    }

    protected function getRootPath()
    {
        return dirname($this->laravel['path']);
    }

}
