<?php

namespace Abaw01\Crud\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Illuminate\Http\Response;

class AdminGenerateCommand extends Command
{
    protected $files;

    protected $signature = 'abaw01:crud:generate {ModelName?}';
    protected $type = 'Controller';

    protected $description = 'This command admin generate';
    
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }
    
    public function fire()
    {

        $pathFileController = $this->getPathFileController($this->argument('ModelName'));
        $pathFileModel      = $this->getPathFileModel($this->argument('ModelName'));
        $pathFileRequest    = $this->getPathFileRequest();
        $pathFileRoute      = $this->getPathFileRoute();
        $pathFileViewList   = $this->getPathFileViewList();

        $pathStubController = $this->getStubController();
        $pathStubModel = $this->getStubModel();
        $pathStubRoute      = $this->getStubRoute();
        $pathStubView       = $this->getStubView();
        $pathStubRequest    = $this->getStubRequest();

        if ($this->files->exists($pathFileController)) {
            $this->error($this->type . ' already exists!');

            return false;
        }

        if ($this->files->exists($pathFileModel)) {
            $this->error($pathFileModel . ' already exists!');

            return false;
        }

        if ($this->files->exists($pathFileViewList.'/'.$this->argument('ModelName'))) {
            $this->error($pathFileViewList.'/'. $this->argument('ModelName') . ' already exists!');

            return false;
        }

        $this->makeDirectory($pathFileController);
        $this->makeDirectory($pathFileViewList);
        $this->makeDirectoryViewModel($pathFileViewList.'/'.$this->argument('ModelName'));
        $this->makeDirectoryRequest($pathFileRequest.'/'.$this->argument('ModelName'));

        $contentController = $this->buildContentController($this->argument('ModelName'), $pathStubController);
        $contentControllerToLower = $this->classToLower($this->argument('ModelName'), $contentController);
        $this->files->put($pathFileController, $contentControllerToLower);
        $this->files->put($pathFileModel, $this->getBladeStubContent($this->argument('ModelName')));
        $this->files->append($pathFileRoute, $this->buildContentRoute($this->argument('ModelName'), $pathStubRoute));
        $ListStubViewFiles = $this->files->files($pathStubView);
            foreach($ListStubViewFiles as $file) {
                $reContext = "";
                if (Str::is("*filtersBody*", $file) == false) {
                    $context = $this->files->get($file);
                    $reContext = $this->buildContent($this->argument('ModelName'), $context);
                }
                if (Str::is("*filtersBody*", $file) == true) {
                    $reContext = $this->getContextFilterBody($file);
                }
                $FileBlade = str_replace($pathStubView, '', $file);
                $newFileBlade = str_replace('.stub', '.php', $FileBlade);
                $this->files->put($pathFileViewList.'/'.$this->argument('ModelName').$newFileBlade, $reContext);
            }
        $ListStubRequests = $this->files->files($pathStubRequest);
            foreach($ListStubRequests as $file) {
                $context = $this->files->get($file);
                $reContext = $this->buildContent($this->argument('ModelName'), $context);
                $FileRequest = str_replace($pathStubRequest, '', $file);
                $newFileRequest = str_replace('.stub', '.php', $FileRequest);
                $this->files->put($pathFileRequest.'/'.$this->argument('ModelName').$newFileRequest, $reContext);
            }
        $this->info('');
        $this->info($this->type.' created successfully.');
    }
    
    protected function getContextFilterBody($file)
    {
        $contextField = "";
        $fields = $this->getModelFields($this->argument('ModelName'));
            if (count($fields) > 0) {
                foreach($fields as $field) {
                $context = $this->files->get($file);
                $reContext = $this->buildContentField($this->argument('ModelName'), $field, $context);
                $contextField = $contextField.$reContext; 
                }
            }

        return $contextField;
    }

    protected function getPathFileController($name)
    {
        return $this->laravel['path'].'/'.'Http/Controllers/Admin/'.$name.'Controller.php';
    }

    protected function getPathFileModel($name)
    {
        return $this->laravel['path'].'/'.'Model/'.$name.'.php';
    }

    protected function getPathFileRoute()
    {
        return $this->laravel['path'].'/'.'Http/routes.php';
    }

    protected function getPathFileViewList()
    {
        return $this->laravel['path'].'/../resources/views/Admin';
    }
    
    protected function getPathFileRequest()
    {
        return $this->laravel['path'].'/Http/Requests';
    }

    protected function makeDirectory($path)
    {
        if (!$this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0777, true, true);
        }
    }
    
    protected function makeDirectoryViewModel($path)
    {
        $this->files->makeDirectory($path, 0777, true, true);
    }

    protected function makeDirectoryRequest($path)
    {
        $this->files->makeDirectory($path, 0777, true, true);
    }

    protected function classToLower($name, $stub)
    {
        $stubLower = $this->replaceClassToLower($stub, $name);
        
        return $stubLower;
    }

    protected function buildContentController($name,$pathStub)
    {
        $stub = $this->files->get($pathStub);

        return $this->replaceNamespace($stub, $name)->replaceClass($stub, $name);
    }

    protected function buildContentRoute($name, $pathStub)
    {
        $stub = $this->files->get($pathStub);
        $stubLower = $this->replaceClassToLower($stub, $name);

        return $this->replaceClassDummy($stubLower, $name);
    }

    protected function buildContent($name, $content)
    {
        $stubret = str_replace('DummyClass', $name, $content);
        $stubretLower = $this->replaceClassToLower($stubret, $name);

        return $stubretLower;
    }
    
    protected function buildContentField($name, $field, $content)
    {
        $stubret = str_replace('DummyClass', $name, $content);
        $stubretLower = $this->replaceClassToLower($stubret, $name);
        $stubretField = str_replace('dummyfield', $field, $stubretLower);

        return $stubretField;
    }

    protected function replaceNamespace(&$stub, $name)
    {
        $stub = str_replace(
            'DummyNamespace', 'App\\Http\\Controllers\\Admin', $stub
        );
        $stub = str_replace(
            'DummyRootNamespace', 'App\\', $stub
        );

        return $this;
    }

    protected function getNamespace($name)
    {
        return trim(implode('\\', array_slice(explode('\\', $name), 0, -1)), '\\');
    }

    protected function replaceClass($stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name);

        return str_replace('DummyClass', $class, $stub);
    }

    protected function replaceClassDummy($stub, $name)
    {
        $stubret = str_replace('DummyClass', $name, $stub);

        return $stubret;
    }

    protected function replaceClassToLower($stub, $name)
    {
        $namelower = Str::lower($name);

        return str_replace('dummyclass', $namelower, $stub);
    }

    protected function getStubController()
    {
        return __DIR__.'/../../Skeleton/app/Http/ModelName/ModelNameController.stub';
    }

    protected function getStubModel()
    {
        return __DIR__.'/../../Skeleton/app/Model/ModelName.blade.php';
    }

    protected function getStubRoute()
    {
        return __DIR__.'/../../Skeleton/app/Http/routes.stub';
    }
    
    protected function getStubView()
    {
        return __DIR__.'/../../Skeleton/resources/views';
    }

    protected function getStubRequest()
    {
        return __DIR__.'/../../Skeleton/app/Http/Requests';
    }

    protected function getModelFields($modelName)
    {
        $path = $this->getPathBaseModel($modelName);

        if ($this->files->exists($path))
        {
            $pathUser = '\\' . $this->laravel->getNamespace() . 'Model\\Base\\' . $modelName;

            $model = new $pathUser;

            return $model->getFillable();
        }

        return array();
    }

    protected function getPathBaseModel($name)
    {

        return $this->laravel['path'].'/'.'Model/Base/'.$name.'.php';
    }

    protected function getRootPath()
    {
        return dirname($this->laravel['path']);
    }

    public function getBladeStubContent($modelName)
    {
        $path = $this->getStubModel();

        $view = 
            $this->laravel->make('view')
            ->file($path, [
                'fields' => 
                $this->getModelFields($modelName), 
                'DummyClass' => $modelName
            ]);

        $content = '<?php' . PHP_EOL . PHP_EOL . $view->render();

        return $content;
    }
}
