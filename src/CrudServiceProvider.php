<?php
    
namespace Abaw01\Crud;

use Illuminate\Support\ServiceProvider;

class CrudServiceProvider extends ServiceProvider
{
    protected $commands = [
        'Abaw01\Crud\Console\Commands\AdminGenerateCommand',
        'Abaw01\Crud\Console\Commands\DeleteGenerateCrudFilesCommand',
    ];

    public function boot()
    {
        $this->commands($this->commands);

    }

    public function register()
    {

    }
}