
namespace App\Model;
use App\Model\Base\{{ $DummyClass }} as Base{{ $DummyClass }};

class {{ $DummyClass }} extends Base{{ $DummyClass }}
{
    static $listFields = [
        @foreach ($fields as $k => $field)
            '{{ $field }}',
        @endforeach
    ];

    static $createFields = [
        @foreach ($fields as $k => $field)
            '{{ $field }}',
        @endforeach
    ];

    static $showFields = [
        @foreach ($fields as $k => $field)
            '{{ $field }}',
        @endforeach
    ];

    static $editFields = [
        @foreach ($fields as $k => $field)
            '{{ $field }}',
        @endforeach
    ];

    public function scopeNoFilter($query)
    {
        return $query;
    }

    public function add{{ $DummyClass }}Filter($query, $params)
    {
        @foreach ($fields as $k => $field)
            if (isset($params['{{ $field }}']) && !empty($params['{{ $field }}'])) {
                $query->where('{{ $field }}', 'LIKE', '%' . $params['{{ $field }}'] . '%');
            }
        @endforeach

        return $query;
    }

    public function scopeFilter($query, $params)
    {
        $query = {{ $DummyClass }}::noFilter($query);
        $query = {{ $DummyClass }}::add{{ $DummyClass }}Filter($query, $params);

        return $query;
    }
}
